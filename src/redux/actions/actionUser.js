import { localServ } from "../../Services/localService";
import { userServ } from "../../Services/userServices";
import { SET_USER } from "../constants/constantsUser";
const setUserLoginSuccess = (successValue) => {
  return {
    type: SET_USER,
    payload: successValue,
  };
};
export const setUserLoginActionServ = (dataLogin,onLoginSuccess,onLoginFail) => {
  return (dispatch) => {
    userServ
      .postLogin(dataLogin)
      .then((res) => {
        localServ.user.set(res.data.content);
        onLoginSuccess();
        console.log("res: ", res);
        dispatch(setUserLoginSuccess(res.data.content));
      })
      .catch((err) => {
        onLoginFail();
        console.log("err: ", err);
      });
  };
};
