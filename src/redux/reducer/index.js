import { combineReducers } from "redux";
import { spinnerReducer } from "./reducerSpinner";
import { userReducer } from "./reducerUser";

export let rootRedcuer = combineReducers({
    userReducer,
    spinnerReducer,
})