export const hideLongString = (desciption, length) => {
    let lengthDescription = desciption.length;
  
    if (lengthDescription > length) {
      return desciption.slice(0, length) + "...";
    } else {
      return desciption;
    }
  };